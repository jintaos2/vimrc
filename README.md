## vim

```
mkdir -p ~/.vim && cd ~/.vim
git clone https://gitlab.engr.illinois.edu/jintaos2/vimrc.git .
```

## neovim
```
mkdir -p ~/.config/nvim && cd ~/.config/nvim
git clone https://gitlab.engr.illinois.edu/jintaos2/vimrc.git .
```
